module AuthAuth

open System
open System.IO
open System.Security.Cryptography

open Chiron
open Operators
open Jose

type UserRequest =
  { Username : string
    Password : string }
  static member Create username password =
    { Username = username
      Password = password }

// Should be compliant to jwt spec. Something like
// exp(ired at), iss(ued by), et cetera.
type Payload =
  { Username  : string
    Devices   : Guid list
    IssuedAt  : int64
    ExpiredAt : int64
    Issuer    : string }

let decoderUserRequest =
  UserRequest.Create
  <!> Json.Decode.required Json.Decode.string "username"
  <*> Json.Decode.required Json.Decode.string "password"

let encoderReplytoken payload jobj =
  let encodeGuidList = Json.Encode.listWith Json.Encode.guid
  jobj
  |> Json.Encode.required Json.Encode.string "name"    payload.Username
  |> Json.Encode.required encodeGuidList     "devices" payload.Devices

[<RequireQualifiedAccessAttribute>]
module Tokenization =

  let private createPass () =
    let cryp = RandomNumberGenerator.Create ()
    let rand = Array.init 32 byte
    cryp.GetBytes rand
    rand

  let private pass () =
    try
      let fin = FileInfo "./secrettoken.txt"
      if not fin.Exists then
        let passp = createPass ()
        File.WriteAllBytes (fin.FullName, passp)
      File.ReadAllBytes fin.FullName
    with
      | :? IOException -> [||]

  let private a2 = JweAlgorithm.A256KW
  let private hs = JweEncryption.A256CBC_HS512

  let private encodeJWT payload =
    JWT.Encode (payload, pass (), a2, hs)
  let private decodeJWT token =
    JWT.Decode (token, pass (), a2, hs)
