module Utilities

open Chiron

let inline toJson (encoder : 'T -> JsonObject -> JsonObject) model =
  model
  |> Json.Encode.jsonObjectWith encoder
  |> Json.format
