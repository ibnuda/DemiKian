module DomainDb

open Marten
open Npgsql

open DomainModel

let createConString host name pass db =
  sprintf "Host=%s;Username=%s;Password=%s;Database=%s" host name pass db
  |> NpgsqlConnectionStringBuilder

let connection = createConString "a" "b" "c" "d" |> string

let store =
  DocumentStore.For (fun doc ->
    doc.AutoCreateSchemaObjects <- AutoCreate.CreateOrUpdate
    doc.Connection connection
    doc.Logger (ConsoleMartenLogger ())
    doc.Schema.For<User>().Index(fun x -> x.Id :> obj) |> ignore
    doc.Schema.For<Device>().Index(fun x -> x.Id :> obj) |> ignore
    doc.Schema.For<Sensor>().Index(fun x -> x.Id :> obj) |> ignore
    doc.Schema.For<Control>().Index(fun x -> x.Id :> obj) |> ignore
    doc.Schema.For<AssignedTo>().Index(fun x -> x.Id :> obj) |> ignore
  )
