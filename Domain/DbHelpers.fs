module DomainDbHelpers

open System

open Marten

open DomainModel
open DomainDb

[<RequireQualifiedAccessAttribute>]
module Read =

  let load<'a> guid =
    use session = store.LightweightSession ()
    Session.loadByGuid<'a> guid session

[<RequireQualifiedAccessAttribute>]
module Write =
  let writeEntity entity =
    use session = store.LightweightSession ()
    Session.storeSingle entity session
    Session.saveChanges session |> ignore
    entity

  let writeEntities entities =
    use session = store.LightweightSession ()
    Session.storeMany entities session
    Session.saveChanges session |> ignore
    entities

[<RequireQualifiedAccessAttribute>]
module Exist =

  let user (userId : Guid) : bool =
    use session = store.LightweightSession ()
    session
    |> Session.query<User>
    |> Queryable.filter <@ fun user -> user.Id = userId @>
    |> Queryable.tryExactlyOne
    |> Option.isSome

  let device (deviceId : Guid) : bool =
    use session = store.LightweightSession ()
    session
    |> Session.query<Device>
    |> Queryable.filter <@ fun device -> device.Id = deviceId @>
    |> Queryable.tryExactlyOne
    |> Option.isSome

  let deviceAssignedToUser deviceId userId =
    use session = store.LightweightSession ()
    session
    |> Session.query<AssignedTo>
    |> Queryable.filter <@ fun assigned -> assigned.DeviceId = deviceId && assigned.UserId = userId @>
    |> Queryable.tryExactlyOne
    |> Option.isSome
