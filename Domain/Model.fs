module DomainModel

open System

open Chiron
open Operators

type User =
  { Id       : Guid
    Username : string
    Password : string }
  static member Create i u p =
    { Id       = i
      Username = u
      Password = p }

type Device =
  { Id          : Guid
    MacAddress  : string
    Name        : string
    Description : string option }
  static member Create i m n d =
    { Id          = i
      MacAddress  = m
      Name        = n
      Description = d }

type Sensor =
  { Id         : Guid
    MacAddress : string
    Temp       : float
    Press      : float
    BarA       : float
    BarB       : float
    RHours     : int
    SError     : string
    SControl   : string }
  static member Create i m t p a b r e c =
    { Id         = i
      MacAddress = m
      Temp       = t
      Press      = p
      BarA       = a
      BarB       = b
      RHours     = r
      SError     = e
      SControl   = c }

type Control =
  { Id         : Guid
    Executor   : string
    MacAddress : string
    Command    : string }
  static member Create i e m c =
    { Id         = i
      Executor   = e
      MacAddress = m
      Command    = c }

type AssignedTo =
  { Id       : Guid
    UserId   : Guid
    DeviceId : Guid }

let decoderUser =
  User.Create
  <!> Json.Decode.required Json.Decode.guid   "id"
  <*> Json.Decode.required Json.Decode.string "username"
  <*> Json.Decode.required Json.Decode.string "password"

let decoderSensor =
  Sensor.Create
  <!> Json.Decode.required Json.Decode.guid   "id"
  <*> Json.Decode.required Json.Decode.string "mac_address"
  <*> Json.Decode.required Json.Decode.float  "temperature"
  <*> Json.Decode.required Json.Decode.float  "pressure"
  <*> Json.Decode.required Json.Decode.float  "bar_a"
  <*> Json.Decode.required Json.Decode.float  "bar_b"
  <*> Json.Decode.required Json.Decode.int    "running_hour"
  <*> Json.Decode.required Json.Decode.string "s_error"
  <*> Json.Decode.required Json.Decode.string "s_control"
